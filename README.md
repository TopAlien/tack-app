﻿#### 1.安装 axios better-scroll
```
cnpm install axios better-scroll --save
```

#### 2.安装stylus stylus-loader 
```
cnpm install stylus stylus-loader --save-dev
```

#### 3.设置缩略路径
>build/webapck.base.conf.js
```
    {
        'src': path.resolve(__dirname, '../src'),
        'common': path.resolve(__dirname, '../src/common'),
        'components': path.resolve(__dirname, '../src/components')
    }
```

#### 4.修改.eslintrc.js 添加规则
>/.eslintrc.js
```
    'semi': ['error', 'always'],
    'no-tabs': 0,
    'indent': 0,
    'space-before-function-paren': 0
```


