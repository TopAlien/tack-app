import Vue from 'vue';
import axios from 'axios';
import App from './App';
import router from './router/index.js';

import 'common/stylus/index.styl';

axios.defaults.baseURL = 'http://localhost:8080/static';
// axios.defaults.baseURL = 'https://wd0518284808kykozz.wilddogio.com/'
Vue.config.productionTip = false;

Vue.prototype.$axios = axios;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
});
